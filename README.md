
TODO
-------------------

Server:
The server handles incoming requests from Player Clients and creates dedicated sockets for them. The 
server is also responsible for running the game LOGIC i.e. it allows players to move/die/win/scoring etc.
The Graphics part of the game is run on the player's computer itself NOT the server.
	- Implement the game logic
		- player connection handler 
		- move
		- game window width/height
		- collision detection
		- command parser
		- command encoder
		- queue chat messages
		- timer for game room before game begins