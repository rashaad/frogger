class GameServer(Object):

	def __init__(self, address, port, numPlayers):
		'''Initialize the GameServer with the ip address, port that it 
		will run on and number of players the game can support.'''

		self.address = address
		self.port = port
		self.numPlayers = numPlayers
		self.players = []
		self.socket = None


	def setup(self):
		'''Setup the server socket connection'''
		#create a new socket for this server
		self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		#bind the server to the address supplied
		self.server.bind(self.address)
		#set the connection to listen for upto 5 connections
		self.server.listen(5)

	def run(self):
		'''Starts the game server'''
		
		#setup the server socket to start listening
		self.setup()

		print('FROGGER SERVER RUNNING CTRL+C TO SHUTDOWN.')

		#start listening for connectiongs
		while True:
		    try:
		        client, address = server.accept()
		        print('CLIENT CONNECTED FROM: ', address)
		        
		        #instantiate a handler to handle the player's connection
		        self.players.append(PlayerHandler(client))

		        #start the player handler thread
		        m.start()
		    except KeyboardInterrupt as e:
		    	#need to loop over all PlayerHandlers and do a safely kill them
		        print("RECEIVED CTRL+C, SHUTTING DOWN...")
		        break

		#shutdown the server        
		server.close()


	def update(self):
		'''Whenever PlayerHandler changes state, this method will be called
		and needs to update every other PlayerHandler of the change.'''




