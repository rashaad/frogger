class Observable(Object):

	def __init__(self):
		self.observers = []

	def addObserver(self, observer):
		'''Add an Observer'''


	def removeObserver(self, observer):
		'''Removes an Observer'''

	def notifyAll(self):
		'''Notifies all Observers listening on this object'''

	def notify(self, observer):
		'''Notifies the specific Observer of the change'''

	def hasChanged(self):
		'''Return true if the object has changed, false otherwise'''

	def setChanged(self):
		'''Set this object's state to changed'''

	def clearChanged(self):
		'''Set this object's state as unchanged'''

	def numObservers(self):
		'''Return the number of Observers listening'''

if __name__ == "__main__":
	print('hello')