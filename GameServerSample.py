import socket
import sys
from threading import Thread
from time import ctime
from time import sleep

class PlayerHandler(Thread):
    def __init__(self, sock):
        Thread.__init__(self, name = "Player Handler")
        self.sock = sock

    def run(self):
        data = ctime() + '\nHave a nice day!'
        self.sock.send(data.encode())
        
        running = 2
        numberMessage = 1
        
        while running:
            clientData = self.sock.recv(2048).decode()
            if clientData:
                print(clientData)
                data = ctime() + ' Message ' + str(numberMessage) + "\n"
                self.sock.send(data.encode())
                numberMessage += 1
                running -= 1
            else:
                running = 0
        
        print("Closing clienthandler")
        sleep(5)
                
        self.sock.close

#setup the server stuff
argv = sys.argv
PORT = int(argv[1])
HOST =  str(argv[2])

ADDRESS = (HOST, PORT)

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
server.bind(ADDRESS)
server.listen(5)

while True:
    print('Waiting for connection ...')
    try:
        client, address = server.accept()
        print('... connected from: ', address)
        
        #create the message handler thread
        m = PlayerHandler(client)
        #start it
        m.start()
    except KeyboardInterrupt as e:
        print("RECEIVED CTRL+C, SHUTTING DOWN...")
        break
    
server.close()

#implement wait for threads to finish here