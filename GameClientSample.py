import socket
import sys

argv = sys.argv
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
host = argv[2]
port = int(argv[1])

#send connection request
sock.connect((host, port))
received = sock.recv(2048)
print(received.decode())

#send message 1
sock.send("HELLO1".encode())
received = sock.recv(2048)
print(received.decode())

#send message2
sock.send("HELLO2".encode())
received = sock.recv(2048)
print(received.decode())

sock.close()